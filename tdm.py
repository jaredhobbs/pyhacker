#!/usr/bin/env python
from subprocess import Popen, PIPE
from time import sleep

SLEEP_INTERVAL = 3
MONITOR_CONNECTED = [
    '/usr/sbin/system_profiler',
    'SPThunderboltDataType',
]
ENABLE_MONITOR = [
    '/usr/bin/osascript',
    '-e',
    'tell application "System Events" to key code 144 using command down',
]


def main():
    display_mode = False
    while True:
        p1 = Popen(MONITOR_CONNECTED, stdout=PIPE)
        p2 = Popen(
            ['/usr/bin/grep', '0xA27'],
            stdin=p1.stdout,
            stdout=PIPE,
        )
        p1.stdout.close()
        out = p2.communicate()[0].strip()
        if out == 'Vendor ID: 0xA27':
            if not display_mode:
                Popen(ENABLE_MONITOR)
                display_mode = True
        else:
            display_mode = False
        sleep(SLEEP_INTERVAL)


if __name__ == '__main__':
    main()

