#!/usr/bin/env python
"""
Organize photos into dated directories.

Requires pyexiv2

Copyright (C) 2012 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import optparse
from hashlib import md5
from shutil import copy2
from random import randint
from collections import defaultdict

import pyexiv2

from logutils import logger

COPIED = set()


def getDate(filename):
    metadata = pyexiv2.ImageMetadata(filename)
    metadata.read()
    return metadata['Exif.Image.DateTime'].value


def createImageMap(dir, fmt, recurse, verbose=False):
    fmap = defaultdict(list)
    for root, dirs, files in os.walk(dir):
        for file in files:
            file = os.path.join(root, file)
            try:
                date = getDate(file)
                if not date:
                    if verbose:
                        logger.error('No date found in exif data for %s. '
                                     'Skipping...', file)
                    continue
                fmap[date.strftime(fmt)].append(file)
            except IOError:
                pass
            except KeyError:
                if verbose:
                    logger.error('No exif data found for %s. Skipping...', file)
            except AttributeError:
                import pdb; pdb.set_trace()
        if not recurse:
            break
    return fmap


def computeHash(a, bytes):
    with open(a, 'rb') as f:
        aHash = md5(f.read(bytes)).digest()
    return aHash


def renameFile(f):
    base, ext = os.path.splitext(f)
    base += '_%d' % randint(9999, 9999999)
    return base + ext


def copyImage(frmImg, toImg, imgSizes, dryRun=False, bytes=10240, verbose=False):
    aSize = os.path.getsize(frmImg)
    frmHash = computeHash(frmImg, bytes)
    if os.path.exists(toImg):
        bSize = os.path.getsize(toImg)
        if aSize == bSize:  # same file size
            if frmHash != computeHash(toImg, bytes):  # not the same
                toImg = renameFile(toImg)
            else:  # frmImg == toImg; do nothing
                return
        else:  # different file sizes
            toImg = renameFile(toImg)
    else:
        for img in imgSizes.get(aSize, []):
            if frmHash == computeHash(img, bytes):  # same image
                return
    if not dryRun:
        copy2(frmImg, toImg)
        COPIED.add(frmImg)
        if verbose:
            logger.info('Copied %s to %s', frmImg, toImg)
    else:
        if verbose:
            logger.info('Would have copied %s to %s', frmImg, toImg)



def copyImages(fmap, outDir, dryRun=False, bytes=10240, verbose=False):
    for date, images in fmap.iteritems():
        root = os.path.join(outDir, date)
        if not os.path.exists(root):
            if not dryRun:
                os.makedirs(root)
                if verbose:
                    logger.info('Created: %s', root)
            else:
                if verbose:
                    logger.info('Would have created: %s', root)
        fsizes = defaultdict(list)
        try:
            for img in os.listdir(root):
                img = os.path.join(root, img)
                fsizes[os.path.getsize(img)].append(img)
        except OSError:
            pass
        for image in images:
            out = os.path.join(root, os.path.basename(image))
            copyImage(image, out, fsizes, dryRun=dryRun, bytes=bytes,
                      verbose=verbose)


def main(options, args):
    inDir, outDir = args
    inDir = os.path.abspath(inDir)
    outDir = os.path.abspath(outDir)
    imageMap = createImageMap(inDir, options.date_format, options.recursive,
                              verbose=options.verbose)
    copyImages(imageMap, outDir, dryRun=options.dry_run, bytes=options.bytes,
               verbose=options.verbose)
    logger.info('Copied %d files', len(COPIED))


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.set_usage("""%prog [OPTIONS] <input_dir> <output_dir>
  This script copies images from <input_dir> into dated directories in
  <output_dir>. If the dated directory doesn't already exist, it will be
  created. If a file of the same name already exists, a hash will be 
  calculated on the first ten kilobytes of the files to determine whether
  they are duplicates. If they are NOT duplicates, the file will be renamed.

  See --help for more information.""")
    parser.add_option('-b', '--bytes', action='store', type='int',
                      default=10240,
                      help='Sets the number of bytes to read from files of '
                           'the same name and length to hash. Defaults to '
                           '%default')
    parser.add_option('-f', '--date-format', action='store', default='%Y%m%d',
                      help='Sets the dated directory date format. Defaults to '
                           '%default')
    parser.add_option('-r', '--recursive', action='store_true', default=False,
                      help='If set, recurse into any subdirectories and copy '
                           'images from there too. Defaults to %default')
    parser.add_option('-n', '--dry-run', action='store_true', default=False,
                      help="If set, don't actually copy images; just "
                           "report what would have been copied.")
    parser.add_option('-v', '--verbose', action='store_true', default=False,
                      help="If set, prints out all status messages.")

    options, args = parser.parse_args()
    if len(args) != 2:
        parser.print_usage()
        sys.exit(1)
    main(options, args)

