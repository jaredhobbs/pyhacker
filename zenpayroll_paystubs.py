#! /usr/bin/env python
"""
This script logs into Zenpayroll and downloads paystubs.

Requires robobrowser.

Copyright (C) 2015 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import json
import sys
from netrc import netrc

from robobrowser.browser import RoboBrowser

EMAIL = ''
ROOT = 'https://manage.zenpayroll.com'
URL = '%s/paystubs' % ROOT
PAYSTUBS = '%s/api/dashboard/paystubs' % ROOT
DOWNLOAD = 'https://zenpayroll.com/payroll_items/%s/paystub.pdf'


def get_browser():
    br = RoboBrowser(
        history=True,
        user_agent='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) '
                   'AppleWebKit/534.48.3 (KHTML, like Gecko) Version/5.1 '
                   'Safari/534.48.3'
    )
    return br


def download_paystubs(br, paystub_dir):
    n = netrc()
    try:
        username, account, password = n.authenticators('manage.zenpayroll.com')
    except TypeError:
        username = password = None
    if None in (username, password):
        raise Exception('Failed to find zenpayroll authentication in ~/.netrc '
                        'file!')
    br.open(URL)
    form = br.get_form(id='new_user')
    form['user[email]'].value = username
    form['user[password]'].value = password
    br.submit_form(form)
    br.open(PAYSTUBS)
    data = json.loads(br.response.text[4:])
    downloads = []
    for paystub in data['employee']['payroll_items']:
        fname = ('%s.pdf' % paystub['check_date']).replace('-', '')
        url = DOWNLOAD % paystub['id']
        br.open(url)
        path = os.path.join(paystub_dir, fname)
        with open(path, 'wb') as f:
            f.write(br.response.content)
        downloads.append(path)
    return downloads


def main(paystub_dir):
    br = get_browser()
    downloads = download_paystubs(br, paystub_dir)
    msg = """The following paystubs have been downloaded:

%s

""" % '\n'.join(downloads)
    attachments = []
    for f in downloads:
        attachments.append('-a%s' % f)
    msg = msg.replace('$', '\$').replace('"', '\"')
    os.system('send_gmail.py %s -s"Paystubs '
              'downloaded" -m"%s" %s' % (EMAIL, msg, ' '.join(attachments)))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: %s <paystub directory>' % sys.argv[0]
        sys.exit(1)
    main(sys.argv[1])
