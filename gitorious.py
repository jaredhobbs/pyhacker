#!/usr/bin/env python
"""
A commandline interface to gitorious

Requires mechanize.

Copyright (C) 2012 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import tempfile
import optparse
import cookielib
from netrc import netrc
from getpass import getpass

import mechanize


SERVER = 'gitorious.org'
USER = None
PASS = None
EMAIL = None

LICENSE_CHOICES = ['None',
                   'Academic Free License v3.0',
                   'MIT License',
                   'BSD License',
                   'Ruby License',
                   'GNU General Public License version 2(GPLv2)',
                   'GNU General Public License version 3 (GPLv3)',
                   'GNU Lesser General Public License (LGPL)',
                   'GNU Affero General Public License (AGPLv3)',
                   'Mozilla Public License 1.0 (MPL)',
                   'Mozilla Public License 1.1 (MPL 1.1)',
                   'Qt Public License (QPL)',
                   'Python License',
                   'zlib/libpng License',
                   'Apache License',
                   'Apple Public Source License',
                   'Perl Artistic License',
                   'Microsoft Permissive License (Ms-PL)',
                   'ISC License',
                   'Lisp Lesser License',
                   'Boost Software License',
                   'Public Domain',
                   'Other Open Source Initiative Approved License',
                   'Other/Proprietary License',
                   'Other/Multiple']


class Path(str):
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        os.remove(self)


def mktemp(dir=None, suffix=''):
    fd, filename = tempfile.mkstemp(dir=dir, suffix=suffix)
    os.close(fd)
    return Path(filename)


def getBrowser():
    """
    Returns a mechanize browser instance.
    """
    br = mechanize.Browser(
        factory=mechanize.RobustFactory(i_want_broken_xhtml_support=True)
    )
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)
    br.set_handle_redirect(True)
    br.set_handle_equiv(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=5)
    br.addheaders = [
        ('User-agent',
         'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) '
         'AppleWebKit/534.48.3 (KHTML, like Gecko) '
         'Version/5.1 Safari/534.48.3'),
    ]
    br.add_handler(mechanize.HTTPSHandler())
    return br


def getSSHKey():
    if not os.path.exists(os.path.expanduser('~/.ssh/id_rsa.pub')):
        os.system('ssh-keygen -t rsa')
    with open(os.path.expanduser('~/.ssh/id_rsa.pub'), 'r') as f:
        k = f.read()
    return k


def getUserPass():
    global USER, PASS, EMAIL
    try:
        n = netrc()
        username, account, password = n.authenticators(SERVER)
        if None in (username, password):
            raise
    except Exception:
        print "Please enter login credentials for %s" % SERVER
        username = raw_input('Email: ')
        password = getpass()
    USER = username.split('@')[0]
    PASS = password
    EMAIL = username


def loginRepo(br):
    br.open('https://%s/login' % SERVER)
    br.select_form(nr=0)
    br.form['email'] = EMAIL
    br.form['password'] = PASS
    response = br.submit()
    soup = mechanize._beautifulsoup.BeautifulSoup(response.read())
    if not soup.head.title.contents[0].startswith('Your dashboard'):
        raise Exception('Failed to login to %s' % br.geturl())


def addSSHKey(br, key):
    """
    Adds an SSH key to the users account.
    Returns True if successful, False otherwise.
    """
    import commands
    with mktemp() as tmpFile:
        with open(tmpFile, 'w') as f:
            f.write(key)
        fprint = commands.getstatusoutput('ssh-keygen -lf %s' % tmpFile)[1]
    fprint = fprint.split()[1]
    br.open('https://%s/~%s/keys' % (SERVER, USER))
    soup = mechanize._beautifulsoup.BeautifulSoup(br.response())
    fprints = set(['%s%s' % (x.contents[0], x.contents[1].contents[0])
                   for x in soup.fetch('code')])
    if fprint in fprints:
        return False
    br.open('https://%s/~%s/keys/new' % (SERVER, USER))
    br.select_form(nr=1)
    br.form['ssh_key[key]'] = key
    br.submit()
    return br.geturl() == 'https://%s/~%s/keys' % (SERVER, USER)


def deleteProject(br, project):
    """
    Deletes a Gitorious project.
    Returns True if successful, False otherwise.
    """
    slug = project.lower().replace(' ', '-')
    try:
        br.open('https://%s/%s/confirm_delete' % (SERVER, slug))
    except mechanize.HTTPError:
        return False
    br.select_form(nr=1)
    br.submit()
    return br.geturl() == 'https://%s/projects' % SERVER


def makeProject(br, project,
                owner=None, labels='', license='None', homeUrl='',
                mailinglistUrl='', bugtrackerUrl='', description=None):
    """
    Creates a new Gitorious project.
    Returns True if successful, False otherwise.
    """
    slug = project.lower().replace(' ', '-')
    owner = owner or USER
    description = description or "Please edit this description"
    br.open('https://%s/projects/new' % SERVER)
    br.select_form(nr=1)
    br.form['project[title]'] = project
    br.form['project[slug]'] = slug
    if owner == USER:
        br.form['project[owner_type]'] = ['User']
    else:
        br.form['project[owner_type]'] = ['Group']
        choices = dict(
            (x.attrs['label'], x.attrs['value'])
            for x in br.form.find_control(name='project[owner_id]').items
        )
        br.form['project[owner_id]'] = [choices[owner]]
    br.form['project[tag_list]'] = labels
    br.form['project[license]'] = [license]
    br.form['project[home_url]'] = homeUrl
    br.form['project[mailinglist_url]'] = mailinglistUrl
    br.form['project[bugtracker_url]'] = bugtrackerUrl
    br.form['project[description]'] = description
    br.submit()
    return br.geturl() == 'https://%s/%s/repositories/new' % (SERVER, slug)


def deleteRepo(br, project, name):
    """
    Deletes a Gitorious repository.
    Returns True if successful, False otherwise.
    """
    slug = project.lower().replace(' ', '-')
    name = name.split('.git')[0]
    try:
        br.open('https://%s/%s/%s/confirm_delete' % (SERVER, slug, name))
    except mechanize.HTTPError:
        return False
    br.select_form(nr=1)
    br.submit()
    return br.geturl() == 'https://%s/%s' % (SERVER, slug)


def makeRepo(br, project, name, description=""):
    """
    Creates a new repository in specified project.
    Returns True if successful, False otherwise.
    """
    br.open('https://%s/%s/repositories/new' % (SERVER, project.lower()))
    br.select_form(nr=1)
    br.form['repository[name]'] = name
    br.form['repository[description]'] = description
    response = br.submit()
    soup = mechanize._beautifulsoup.BeautifulSoup(response.read())
    return soup.head.title.contents[0].startswith(name)


def main(options, args):
    global SERVER
    br = getBrowser()
    SERVER = options.server
    loginRepo(br)
    if options.add_ssh_key:
        ret = addSSHKey(br, getSSHKey())
        if ret:
            print 'Successfully added your SSH key to tig.'
        else:
            print 'Failed to add your SSH key to %s. ' % SERVER +\
                  'Maybe it has already been added?'
    if options.delete_project is not None:
        ret = deleteProject(br, options.delete_project)
        if ret:
            print 'Successfully deleted project "%s"' % options.delete_project
        else:
            print 'Failed to delete project "%s"' % options.delete_project
    if options.make_project is not None:
        ret = makeProject(
            br,
            options.make_project,
            owner=options.project_owner,
            labels=options.project_labels,
            license=options.project_license,
            homeUrl=options.project_home_url,
            mailinglistUrl=options.project_mailinglist_url,
            bugtrackerUrl=options.project_bugtracker_url,
            description=options.project_description
        )
        if ret:
            print 'Successfully created project "%s"' % options.make_project
        else:
            print 'Failed to create project "%s".' % options.make_project,
            print 'Perhaps it already exists?'
    msg = 'The project name of the repository must be specified with the ' + \
          '--project-name option'
    if options.delete_repo is not None:
        if options.project_name is None:
            raise optparse.OptionError(msg, '--delete-repo')
        ret = deleteRepo(br, options.project_name, options.delete_repo)
        if ret:
            print 'Successfully deleted repository "%s" in project "%s"' % \
                  (options.delete_repo, options.project_name)
        else:
            print 'Failed to delete repository "%s" in project "%s"' % \
                  (options.delete_repo, options.project_name)
    if options.make_repo is not None:
        if options.project_name is None:
            raise optparse.OptionError(msg, '--make-repo')
        ret = makeRepo(
            br,
            options.project_name,
            options.make_repo,
            description=options.repo_description
        )
        if ret:
            print 'Successfully created repository "%s" in project "%s"' % \
                  (options.make_repo, options.project_name)
        else:
            print 'Failed to create repository "%s" in project "%s"' % \
                  (options.make_repo, options.project_name)


if __name__ == '__main__':
    getUserPass()
    parser = optparse.OptionParser()
    parser.set_usage("""%prog [OPTIONS]
  A Python interface to Gitorious

  To use without being prompted for a password every time, create an entry in
  ~/.netrc that looks like:
     machine <gitorious server>
     login <email address>
     password <password>
  Make sure to chmod the file to 600 so only you can read it
  (chmod 600 ~/.netrc)

  See --help for more details.""")
    parser.add_option(
        '--server', action='store', default=SERVER,
        help='Sets the Gitorious server address. Defaults to %default'
    )
    parser.add_option(
        '--add-ssh-key', action='store_true', default=False,
        help="Use this option to add your ssh key to your gitorious account."
    )

    project = optparse.OptionGroup(
        parser, 'Project functions',
        "These functions allow for the creation/deletion of projects."
    )
    project.add_option(
        '--delete-project', action='store', default=None,
        help="Delete an existing Gitorious project with specified name."
             "This will also delete all repositories under the project.",
        metavar='PROJECT_NAME'
    )
    project.add_option(
        '--make-project', action='store', default=None,
        help="Create a new Gitorious project with specified name.",
        metavar='PROJECT_NAME'
    )
    project.add_option(
        '--project-description', action='store',
        default=None, help="Set the project description."
    )
    project.add_option(
        '--project-owner', action='store',
        default=USER, help="Set the project owner. This can be yourself "
                           "(default) or a team name. The team name must "
                           "exist and you must be a member of the team to "
                           "use that option. Defaults to %default."
    )
    project.add_option(
        '--project-labels', action='store', default='',
        help="Add labels to the project. The argument to this this option "
             "should be a space separated list of labels."
    )
    project.add_option(
        '--project-license', action='store', default='None',
        help="Specify the license of the project. Defaults to %default.",
        choices=LICENSE_CHOICES
    )
    project.add_option(
        '--project-home-url', action='store', default='',
        help="Add a home URL to the project."
    )
    project.add_option(
        '--project-mailinglist-url', action='store', default='',
        help="Add a mailinglist URL to the project."
    )
    project.add_option(
        '--project-bugtracker-url', action='store', default='',
        help="Add a bugtracker URL to the project."
    )
    parser.add_option_group(project)

    repo = optparse.OptionGroup(
        parser, 'Repository functions',
        "These functions allow for the creation/deletion of repositories."
        "These functions all require the --project-name option "
        "to be specified."
    )
    repo.add_option(
        '--project-name', action='store', default=None,
        help="The name of the Gitorious project that encapsulates the "
             "repositories you're dealing with."
    )
    repo.add_option(
        '--delete-repo', action='store', default=None,
        help="Delete an existing Gitorious repo with specified name."
             "Requires the --project-name option to also be specified.",
        metavar='REPO_NAME'
    )
    repo.add_option(
        '--make-repo', action='store', default=None,
        help="Create a new Gitorious repository with specified name."
             "Requires the --project-name option to also be specified.",
        metavar='REPO_NAME'
    )
    repo.add_option(
        '--repo-description', action='store',
        default="", help="Set the repository description."
    )
    parser.add_option_group(repo)

    options, args = parser.parse_args()
    if all(options.__dict__[k] == v for k, v in parser.defaults.iteritems()):
        parser.print_usage()
        sys.exit()
    main(options, args)
