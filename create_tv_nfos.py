#! /usr/bin/env python
"""
This script creates an NFO file suitable to use with the Boxee Box by querying
the TVDB.

Requires mechanize.

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import re
import sys
import cookielib

import mechanize

TVDB = 'http://thetvdb.com/?tab=series&id=%s'

def debug(t, v, tb):
    import traceback, pdb
    traceback.print_exception(t, v, tb)
    print
    pdb.pm()

sys.excepthook = debug

def getBrowser():
    br = mechanize.Browser()
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)
    br.set_handle_redirect(True)
    br.set_handle_equiv(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=5)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6')]
    return br

def getShowID():
    tvdb = 'http://thetvdb.com/?string=%s&searchseriesid=&tab=listseries&function=Search'
    default = os.path.basename(os.getcwd())
    br = getBrowser()
    while True:
        search = raw_input('Enter TV show name [default=%s]: ' % default) or default
        br.open(tvdb % search.replace(' ', '+'))
        soup = mechanize._beautifulsoup.BeautifulSoup(br.response().read())
        row = soup.body.table('tr')[5].td.contents[0]
        if not row.startswith('No Series Found'):
            rows = [x.td for x in soup.body.table('tr')[5:-3]]
            shows = [x.contents[0].contents[0] for x in rows]
            ids = [x.contents[0]['href'].split('&')[1].split('=')[-1] for x in rows]
            showToIds = dict(zip(shows, ids))
            break
        print "No Series Found"
    for i, show in enumerate(sorted(showToIds), 1):
        print '%d. %s' % (i, show)
    while True:
        show = raw_input('Choose a TV show: ')
        try:
            show = showToIds[sorted(showToIds)[int(show) - 1]]
            break
        except:
            pass
    return show

def writeTopLevelNFO(response, id):
    soup = mechanize._beautifulsoup.BeautifulSoup(response.read())
    thumb = ''
    for img in soup.body('img', attrs={'class': 'banner'}):
        if 'posters' in img.attrMap['src']:
            thumb = 'http://thetvdb.com%s' % img.attrMap['src']
            break
    g = soup.body('div', attrs={'id': 'content'})[1].table('td')[-1].contents
    if g:
        g = g[0].split(',')
    genres = '\n'.join(['<genre>%s</genre>' % g.strip() for g in g])
    txt = """<tvshow>
 <title>%s</title>
 <plot>%s</plot>
 <thumb>%s</thumb>
 <id>%s</id>
 %s
</tvshow>

""" % (soup.body('h1')[0].contents[0],
       soup.body('div', attrs={'id': 'content'})[0].contents[-1].strip(),
       thumb, id, genres)
    with open('tvshow.nfo', 'w') as f:
        f.write(txt)

def writeNFO(response, filename, season, episode):
    soup = mechanize._beautifulsoup.BeautifulSoup(response.read())
    plot = soup.body('textarea', attrs={'style': 'display: inline'})[0].contents
    if plot:
        plot = plot[0]
    else:
        plot = ''
    txt = """<episodedetails>
 <title>%s</title>
 <season>%s</season>
 <episode>%s</episode>
 <plot>%s</plot>
 <aired>%s</aired>
</episodedetails>

""" % (soup.body('input', attrs={'style': 'display: inline'})[0].attrMap['value'],
       season, episode, plot,
       soup.body('input', attrs={'name': 'FirstAired'})[0].attrMap['value'])
    with open(filename, 'w') as f:
        f.write(txt)

def parseOutSeasonEpisode(fname):
    a = re.search('S[0-9]+E[0-9]+', fname)
    if not a:
        print "Couldn't determine season/episode for %s" % fname
        return None, None
    return a.group().split('S')[-1].split('E')

def main(tvShow):
    files = [f for f in os.listdir('.') if f.endswith(('mp4', 'avi', 'mkv', 'flv'))]
    for d in os.listdir('.'):
        if not d.startswith('S'):
            continue
        if not os.path.isdir(d):
            continue
        files.extend(['%s/%s' % (d, f) for f in os.listdir(d) if f.endswith(('mp4', 'avi', 'mkv', 'flv'))])
    br = getBrowser()
    br.open(TVDB % tvShow)
    writeTopLevelNFO(br.response(), tvShow)
    br.follow_link(text='All')
    for f in files:
        s, e = parseOutSeasonEpisode(f)
        if None in (s, e):
            continue
        s = s.lstrip('0')
        e = e.lstrip('0')
        text = '%s - %s' % (s, e)
        if s == '':
            s = 'Specials'
            br.back()
            br.follow_link(text='Specials')
            br.follow_link(text=e)
            response = writeNFO(br.response(), os.path.splitext(f)[0] + '.nfo', s, e)
            br.back()
            br.back()
            br.follow_link(text='All')
        else:
            br.follow_link(text=text)
            writeNFO(br.response(), os.path.splitext(f)[0] + '.nfo', s, e)
            br.back()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.argv.append(getShowID())
    main(sys.argv[1])
