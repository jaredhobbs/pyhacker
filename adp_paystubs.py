#! /usr/bin/env python
"""
This script logs into ADP and downloads paystubs.

Requires selenium and requests.

Copyright (C) 2016 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import atexit
import os
import sys
import time
from datetime import datetime
from netrc import netrc

import requests

from selenium.webdriver.common.by import By
from selenium import webdriver

CHROME_DRIVER = os.path.join(os.environ['VIRTUAL_ENV'], 'chromedriver-Darwin')
EMAIL = ''
LOGIN = 'https://workforcenow.adp.com/public/index.htm'


def get_browser():
    br = webdriver.Chrome(CHROME_DRIVER)
    return br


def logout(br):
    try:
        br.find_element(By.CLASS_NAME, 'fa-sign-out').click()
        br.find_element(By.ID, 'revit_form_Button_1').click()
    except:
        pass


def wait_for(condition_function):
    start_time = time.time()
    while time.time() < start_time + 10:
        if condition_function():
            return True
        time.sleep(0.1)
    raise Exception(
        'Timeout waiting for {}'.format(condition_function.__name__)
    )


def download_paystubs(br, paystub_dir):
    n = netrc()
    try:
        username, account, password = n.authenticators('adp.com')
    except TypeError:
        username = password = None
    if None in (username, password):
        raise Exception('Failed to find namely authentication in ~/.netrc '
                        'file!')
    br.get(LOGIN)
    el = br.find_element(By.NAME, "USER")
    el.send_keys(username)
    el = br.find_element(By.NAME, "PASSWORD")
    el.send_keys(password)
    el.submit()
    el = br.find_element(By.LINK_TEXT, "Pay Statement")
    el.click()
    wait_for(lambda: br.find_elements(By.ID, "tableLink"))
    el = br.find_element(By.ID, "tableLink")
    el.click()
    wait_for(
        lambda: br.find_elements(By.ID, 'payStatementDataGrid_rows_tbody')
    )
    tbody = br.find_element(By.ID, "payStatementDataGrid_rows_tbody")
    downloads = []
    for row in tbody.find_elements(By.TAG_NAME, 'tr')[1:]:
        txt = row.text.split()[0]
        pay_date = datetime.strptime(
            txt, '%m/%d/%Y'
        ).strftime('%Y%m%d')
        fname = '%s.pdf' % pay_date
        el = row.find_element(By.LINK_TEXT, txt)
        time.sleep(1)
        el.click()
        wait_for(
            lambda: br.find_element(
                By.ID,
                "iFrameTag"
            ).get_attribute('src')
        )
        pdf_url = br.find_element(By.ID, "iFrameTag").get_attribute('src')
        pdf = requests.get(
            pdf_url,
            cookies={cookie['name']: '%(value)s' % cookie
                     for cookie in br.get_cookies()}
        )
        path = os.path.join(paystub_dir, fname)
        with open(path, 'wb') as f:
            f.write(pdf.content)
        downloads.append(path)
        dialog = br.find_element(By.ID, 'getCheckDocView')
        el = dialog.find_element(By.CLASS_NAME, 'dijitDialogCloseIcon')
        el.click()
    return downloads


def main(paystub_dir):
    br = get_browser()
    atexit.register(logout, br)
    downloads = download_paystubs(br, paystub_dir)
    msg = """The following paystubs have been downloaded:

%s

""" % '\n'.join(downloads)
    attachments = []
    for f in downloads:
        attachments.append('-a%s' % f)
    msg = msg.replace('$', '\$').replace('"', '\"')
    os.system('send_gmail.py %s -s"Paystubs '
              'downloaded" -m"%s" %s' % (EMAIL, msg, ' '.join(attachments)))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: %s <paystub directory>' % sys.argv[0]
        sys.exit(1)
    main(sys.argv[1])

