#! /usr/bin/env python
"""
This script logs into Paychex and downloads paystubs.

Requires mechanize.

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import re
import sys
import cookielib
from netrc import netrc
from getpass import getpass
from datetime import datetime

import mechanize

def debug(t, v, tb):
    import traceback, pdb
    traceback.print_exception(t, v, tb)
    print
    pdb.pm()

sys.excepthook = debug

BRANCH = ''
CLIENT = ''
EMAIL = ''

URL = 'https://eservices.paychex.com/secure/'
URL2 = 'https://eservices.paychex.com/secure/hub_home.aspx'

def getBrowser():
    br = mechanize.Browser(factory=mechanize.RobustFactory(i_want_broken_xhtml_support=True))
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)
    br.set_handle_redirect(True)
    br.set_handle_equiv(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=5)
    br.addheaders = [('User-agent',
                      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/534.48.3 (KHTML, like Gecko) Version/5.1 Safari/534.48.3'),]
    br.add_handler(mechanize.HTTPSHandler())
    return br

def selectForm(br, name, url):
    soup = mechanize._beautifulsoup.BeautifulSoup(br.response().get_data())
    for item in soup.fetch('script'):
        item.contents = []
    html = soup.prettify()
    response = mechanize.make_response(html, [("Content-Type", "text/html")], url, 200, "OK")
    br.set_response(response)
    br.select_form(name=name)

def downloadPaystub(br, paystubdir):
    n = netrc()
    try:
        username, account, password = n.authenticators('eservices.paychex.com')
        if None in (username, password):
            raise Exception('Failed to find Paychex authentication in ~/.netrc file!')
    except:
        username = raw_input('Paychex username: ')
        password = getpass()
    br.open(URL)
    br.select_form(name='form1')
    br.form['txtBranch'] = BRANCH or raw_input('Branch: ')
    br.form['txtClient'] = CLIENT or raw_input('Client: ')
    br.form['txtUserName'] = username
    br.form['txtPassword'] = password
    response = br.submit()
    soup = mechanize._beautifulsoup.BeautifulSoup(response.read())
    br.open(URL2)
    soup = mechanize._beautifulsoup.BeautifulSoup(br.response().read())
    br.follow_link(text='Check History')
    soup = mechanize._beautifulsoup.BeautifulSoup(br.response().read())
    year = datetime.now().year
    links = [x for x in br.links() if re.search('^[0-9]{2}/[0-9]{2}/%d$' % year, x.text) is not None]
    files = set(os.listdir(paystubdir))
    downloads = []
    for link in links:
        fname = datetime.strptime(link.text, '%m/%d/%Y').strftime('%Y%m%d.pdf')
        if fname in files:
            continue
        url = URL + link.url.split("window.open('")[-1].split("',null")[0]
        downloads.append(br.retrieve(url, os.path.join(paystubdir, fname))[0])
    br.follow_link(text='Attendance/Time-Off')
    soup = mechanize._beautifulsoup.BeautifulSoup(br.response().read())
    table = soup.fetch('table', attrs={'id': 'ctl00_DataSection_DgTimeOffSummary'})[0]
    vacation = table.fetch('td')[4].contents[0]
    holiday = table.fetch('td')[7].contents[0]
    return vacation, holiday, downloads

def main(paystubdir):
    br = getBrowser()
    vacation, holiday, downloads = downloadPaystub(br, paystubdir)
    if downloads:
        d = 'The following paystubs have been downloaded:\n%s' % '\n'.join(downloads)
    else:
        d = 'All paystubs already exist; no new paystubs downloaded.'
    msg = """%s

Vacation hours available: %s
Holiday hours available: %s

""" % (d, vacation, holiday)
    msg = msg.replace('$', '\$').replace('"', '\"')
    print msg
    os.system('send_gmail.py %s -s"Paystubs downloaded" -m"%s"' % (EMAIL, msg))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: %s <paystub directory>' % sys.argv[0]
        sys.exit(1)
    main(sys.argv[1])

