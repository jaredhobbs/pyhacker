#!/usr/bin/env python
"""
Organize music into Artist/Album/Track format.

Requires mutagen

Copyright (C) 2012 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import optparse
from hashlib import md5
from shutil import copy2
from random import randint
from collections import defaultdict

import mutagen
from mutagen.easyid3 import EasyID3

from logutils import logger

COPIED = set()


def getNewFilename(filename):
    ext = os.path.splitext(filename)[1]
    m = EasyID3(filename)
    n = {'ext': ext}
    for field in ('artist', 'album', 'tracknumber', 'title'):
        n[field] = list(m[field])[0]
    try:
        n['tracknumber'] = '%.02d' % int(n['tracknumber'])
    except ValueError:
        pass
    return os.path.join('%(artist)s', '%(album)s') % n,\
        '%(tracknumber)s %(title)s%(ext)s' % n


def createFileMap(dir, recurse, verbose=False):
    fmap = defaultdict(list)
    nmap = {}
    for root, dirs, files in os.walk(dir):
        for file in files:
            file = os.path.join(root, file)
            try:
                newDir, newFile = getNewFilename(file)
                nmap[file] = newFile
                fmap[newDir].append(file)
            except IOError:
                pass
            except KeyError:
                pass
            except mutagen.id3.ID3NoHeaderError:
                if verbose:
                    logger.error('No ID3 data found for %s. Skipping...', file)
            except AttributeError:
                import pdb; pdb.set_trace()
        if not recurse:
            break
    return fmap, nmap


def computeHash(a, bytes):
    with open(a, 'rb') as f:
        aHash = md5(f.read(bytes)).digest()
    return aHash


def renameFile(f):
    base, ext = os.path.splitext(f)
    base += '_%d' % randint(9999, 9999999)
    return base + ext


def copyFile(frmMfile, toMfile, mfileSizes, dryRun=False, bytes=10240,
             verbose=False):
    aSize = os.path.getsize(frmMfile)
    frmHash = computeHash(frmMfile, bytes)
    if os.path.exists(toMfile):
        bSize = os.path.getsize(toMfile)
        if aSize == bSize:  # same file size
            if frmHash != computeHash(toMfile, bytes):  # not the same
                toMfile = renameFile(toMfile)
            else:  # frmMfile == toMfile; do nothing
                return
        else:  # different file sizes
            toMfile = renameFile(toMfile)
    else:
        for mfile in mfileSizes.get(aSize, []):
            if frmHash == computeHash(mfile, bytes):  # same mFile
                return
    if not dryRun:
        copy2(frmMfile, toMfile)
        COPIED.add(frmMfile)
        if verbose:
            logger.info('Copied %s to %s', frmMfile, toMfile)
    else:
        if verbose:
            logger.info('Would have copied %s to %s', frmMfile, toMfile)



def copyFiles(fmap, nameMap, outDir, dryRun=False, bytes=10240, verbose=False):
    for newDir, mFiles in fmap.iteritems():
        root = os.path.join(outDir, newDir)
        if not os.path.exists(root):
            if not dryRun:
                os.makedirs(root)
                logger.info('Created: %s', root)
            else:
                logger.info('Would have created: %s', root)
        fsizes = defaultdict(list)
        try:
            for mfile in os.listdir(root):
                mfile = os.path.join(root, mfile)
                fsizes[os.path.getsize(mfile)].append(mfile)
        except OSError:
            pass
        for mFile in mFiles:
            out = os.path.join(root, nameMap[mFile])
            copyFile(mFile, out, fsizes, dryRun=dryRun, bytes=bytes,
                     verbose=verbose)


def main(options, args):
    inDir, outDir = args
    inDir = os.path.abspath(inDir)
    outDir = os.path.abspath(outDir)
    mFileMap, nameMap = createFileMap(inDir, options.recursive,
                                      verbose=options.verbose)
    copyFiles(mFileMap, nameMap, outDir, dryRun=options.dry_run,
              bytes=options.bytes, verbose=options.verbose)
    logger.info('Copied %d files', len(COPIED))


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.set_usage("""%prog [OPTIONS] <input_dir> <output_dir>
  This script copies music from <input_dir> into artist/album directories in
  <output_dir>. If the artist/album directory doesn't already exist, it will be
  created. If a file of the same name already exists, a hash will be 
  calculated on the first ten kilobytes of the files to determine whether
  they are duplicates. If they are NOT duplicates, the file will be renamed.

  See --help for more information.""")
    parser.add_option('-b', '--bytes', action='store', type='int',
                      default=10240,
                      help='Sets the number of bytes to read from files of '
                           'the same name and length to hash. Defaults to '
                           '%default')
    parser.add_option('-r', '--recursive', action='store_true', default=False,
                      help='If set, recurse into any subdirectories and copy '
                           'music from there too. Defaults to %default')
    parser.add_option('-n', '--dry-run', action='store_true', default=False,
                      help="If set, don't actually copy music; just "
                           "report what would have been copied.")
    parser.add_option('-v', '--verbose', action='store_true', default=False,
                      help="If set, prints out all status messages.")

    options, args = parser.parse_args()
    if len(args) != 2:
        parser.print_usage()
        sys.exit(1)
    main(options, args)

